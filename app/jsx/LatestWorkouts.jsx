import React from 'react';

var DaysAgo = require('./DaysAgo.jsx');

var LatestWorkouts = React.createClass({
  render: function() {
    var workouts = [];

    for (var i = 0; i < this.props.allHistoricalWorkouts.length; i++){
      workouts.push(
        <div key={i}><DaysAgo date={this.props.allHistoricalWorkouts[i].date} />: {this.props.allHistoricalWorkouts[i].routine.name}</div>
      );
    }

    return (
     <div>
       <h1>Latest Workouts</h1>
        {workouts}
     </div>
    );
  }
});

module.exports = LatestWorkouts;
