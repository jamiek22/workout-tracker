import React from 'react';

var Exercise = React.createClass({
  handleChangeReps(e){
    this.props.handleChangeReps(e.target.value);
  },

  handleChangeWeight(e){
    this.props.handleChangeWeight(e.target.value);
  },

  handleKeyPress(e){
    if (e.key === 'Enter') {
      this.props.pressedEnter();
    }
  },

  render: function(){
    var warmup = this.props.isWarmup ? <div className="callout">Warm-up exercise (60% effort)</div> : null;

    return (
      <div>
        {warmup}
        <div className="row">
          <div className="left">Reps: <input type="number" name="reps" value={this.props.reps} onChange={this.handleChangeReps} /></div>
          <div className="right">Weight: <input type="number" name="weight" id="weight" value={this.props.weight} onChange={this.handleChangeWeight} onKeyPress={this.handleKeyPress} /></div>
        </div>
      </div>
    );
  }
});

module.exports = Exercise;
