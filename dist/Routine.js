var Routine = function(n, className = 'primary'){
	this.name = n || "";
	this.actions = [];	//an array of objects (Supersets, Exercises and Pauses)
	this.className = className;		//match boostrap button classes (primary, danger, etc)

	//this could be private
	this.addAction = function(action){
		this.actions.push(action);
	};

	this.addExercise = function(name, sets, reps, isWarmup, isSuperSet, isLastExercise){
		isWarmup = isWarmup || false;
		isSuperSet = isSuperSet || false;
		isLastExercise = isLastExercise || false;

		//for each set, then add a pause
		for (var i = 1; i <= sets; i++){
			this.addAction(new Exercise(name + ' ' + i + '/' + sets, sets, reps, isWarmup));
			//Dont show pause if: its a superset (its added separately), or its the last exercise in a routine and the last set.
			if (isSuperSet || (isLastExercise && i == sets)){
				//do nothing
			}
			else {
				this.addAction(new Pause());
			}
		}
	};

	this.addSuperSet = function(exercise1Name, exercise2Name, s, r){
		for (var i = 1; i <= s; i++){
			this.addAction(new Exercise(exercise1Name + ' ' + i + '/' + s, 1, r));
			this.addAction(new Exercise(exercise2Name + ' ' + i + '/' + s, 1, r));
			this.addAction(new Pause());
		}
	};

	this.addCardio = function(){
		this.addAction(new Cardio());
	};

	this.addStretching = function(){
		this.addAction(new Stretching());
	};
};
