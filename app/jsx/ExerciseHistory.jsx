import React from 'react';

var DaysAgo = require('./DaysAgo.jsx');

var ExerciseHistory = React.createClass({
  removeStepFromString: function(str){
    if (str) {
      return str.substr(0, str.length - 3);
    }
    return '';
    // console.log('7', str);
  },

  render: function(){
    var history = [];        //all the historical workouts of this type
    var historyItem;         //a single historical workout of this type (gets added to 'history' array)
    var thisCopy = this;

    /*
    This works, it outputs:
    8 days ago: Sets: 3, Reps: 15, Weight: 80

    this.props.allHistoricalWorkouts.forEach(function(historicalWorkout, index){
      if (historicalWorkout.routine.name === thisCopy.props.newWorkout.routine.name){
        history.push(<div key={index}><DaysAgo date={historicalWorkout.date} />: Sets: {historicalWorkout.routine.actions[thisCopy.props.stepNumber].sets},
          Reps: {historicalWorkout.routine.actions[thisCopy.props.stepNumber].reps},
          Weight: {historicalWorkout.routine.actions[thisCopy.props.stepNumber].weight}</div>);
      }
    });
    */

    /*
    Make it output:
    8 days ago: 15*80, 15*85, 15*90 (bolding the active one)
    */
    this.props.allHistoricalWorkouts.forEach(function(historicalWorkout, index){
      if (historicalWorkout.routine.name === thisCopy.props.newWorkout.routine.name){
        historyItem = [];
        historyItem.push(<DaysAgo date={historicalWorkout.date} />);
        historyItem.push(<span>: </span>);

        historicalWorkout.routine.actions.forEach(function(action, index2){
          var historicalActionName = thisCopy.removeStepFromString(action.name);
          var currentActionName = thisCopy.removeStepFromString(thisCopy.props.newWorkout.routine.actions[thisCopy.props.stepNumber].name);
          var fontWeight;
          if (historicalActionName === currentActionName) {
            if (index2 === thisCopy.props.stepNumber) {
              fontWeight = { fontWeight: "bold" };
            } else {
              fontWeight = null;
            }
            if (historyItem.length > 2) {
              historyItem.push(<span>, </span>);
            }
            historyItem.push(<span style={fontWeight}>{action.reps} x {action.weight}</span>);
            //too much of a hassle to detect if need comma
          }
        });
        history.push(<div>{historyItem}</div>);
      }
    });

    return (
        <div>
          <h2>Exercise History</h2>
          {history}
        </div>
      );
  }
});

module.exports = ExerciseHistory;
