/*
  Props:
  loadedRoutine
  loadedRoutineStep
  routines
*/

import React from 'react';

var Countdown = require('./Countdown.jsx');
var Exercise = require('./Exercise.jsx');
var ExerciseHistory = require('./ExerciseHistory.jsx');
var StepHeading = require('./StepHeading.jsx');
var NextButton = require('./NextButton.jsx');
var PreviousButton = require('./PreviousButton.jsx');

var RoutineStep = React.createClass({
  render: function() {
    if (this.props.loadedRoutine === null) {
      return <div></div>;
    } else {
      //comes in here every time press 'next', but if it is back to back timer ones, the timer is not restarting
      // load the routine
      // using .reduce() wasn't working for some reason (skips first element in array)
      var currentRoutine = {};
      for (var i = 0; i < this.props.routines.length; i++){
        if (this.props.routines[i].name === this.props.loadedRoutine) {
          currentRoutine = this.props.routines[i];
          break;
        }
      }

      //get the exercise info at this step
      var currentAction = currentRoutine.actions[this.props.loadedRoutineStep]; //Supersets, Exercises and Pauses
      var countdown = null;
      var exercise = null;
      var exerciseHistory = null;
      switch(currentAction.name){
        case 'Cardio':
        case 'Pause':
        case 'Stretching':
        //clear a timer (needed in case there are two in a row; its not re-rendering)

          countdown = <Countdown duration={currentAction.duration} />;
          break;
        default:
          exercise = <Exercise reps={currentAction.reps}
                               weight={currentAction.weight}
                               handleChangeReps={this.props.handleChangeReps}
                               handleChangeWeight={this.props.handleChangeWeight}
                               isWarmup={currentAction.isWarmup}
                               pressedEnter={this.props.goToNextStep}
                     />;
          exerciseHistory = <ExerciseHistory allHistoricalWorkouts={this.props.allHistoricalWorkouts}
                                             newWorkout={this.props.newWorkout}
                                             stepNumber={this.props.loadedRoutineStep} />
      }

      return (
        <div>
          <StepHeading name={currentAction.name}
                       stepNumber={this.props.loadedRoutineStep + 1}
                       totalSteps={currentRoutine.actions.length} />
          <div className="row">
            <NextButton goToNextStep={this.props.goToNextStep}
                        stepNumber={this.props.loadedRoutineStep + 1}
                        totalSteps={currentRoutine.actions.length}
                        saveWorkout={this.props.saveWorkout} />
            <PreviousButton goToPreviousStep={this.props.goToPreviousStep}
                        stepNumber={this.props.loadedRoutineStep + 1}
                        totalSteps={currentRoutine.actions.length} />
          </div>

          {countdown}
          {exercise}
          {exerciseHistory}
        </div>
      );
    }
  }
});

module.exports = RoutineStep;
