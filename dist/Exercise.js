var Exercise = function(_name, _sets, _reps, _isWarmup, _weight){
	this.name = _name || "";
	this.sets = _sets || 0;
	this.reps = _reps || 0;
	this.isWarmup = _isWarmup || false;
	this.weight = _weight || '';	//how much weight did (only used when recording a completed exercise)
};
