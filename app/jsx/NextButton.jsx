import React from 'react';

var NextButton = React.createClass({
  isLastStep: function(){
    return this.props.stepNumber === this.props.totalSteps;
  },

  handleClick: function(){
    if (this.isLastStep()) {
      this.props.saveWorkout();
    } else {
      this.props.goToNextStep();
    }
  },

  render: function(){
    var buttonText = this.isLastStep() ? 'Finish' : 'Next';

    return (
      <button type="button" className="button btn-primary" onClick={this.handleClick}>{buttonText} »</button>
    );
  }
});

module.exports = NextButton;
