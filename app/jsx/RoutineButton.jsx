/*
  Show the blue routine button.  Click it to start the routine.
*/

import React from 'react';

var RoutineButton = React.createClass({
  handleClick: function(){
    this.props.startRoutine(this.props.type);
  },

  render: function() {
    return (
     <div className={`button ${this.props.className}`} onClick={this.handleClick}>{this.props.type}</div>
    );
  }
});

module.exports = RoutineButton;
