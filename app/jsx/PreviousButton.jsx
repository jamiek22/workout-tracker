import React from 'react';

var PreviousButton = React.createClass({
  handleClick: function(){
    this.props.goToPreviousStep();
  },

  render: function(){
    var buttonText = (this.props.stepNumber === 1) ? 'Cancel' : 'Previous';
    return (
      <button type="button" className="button btn-primary previous" onClick={this.handleClick}>« {buttonText}</button>
    );
  }
});

module.exports = PreviousButton;
