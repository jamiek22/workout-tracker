/*
  If no routine is started, show the list of routines.
  Otherwise, show nothing.
*/

import React from 'react';

var RoutineButton = require('./RoutineButton.jsx');
var LatestWorkouts = require('./LatestWorkouts.jsx');

var RoutinesHome = React.createClass({
  render: function() {
    var routines = [];

    for (var i = 0; i < this.props.routines.length; i++) {
      routines.push(<RoutineButton key={i} startRoutine={this.props.startRoutine} type={this.props.routines[i].name} className={`btn-${this.props.routines[i].className}`} />);
    }

    if (this.props.loadedRoutine === null) {
      return (
        <div>
          <h1>Routines</h1>
          {routines}
          <LatestWorkouts allHistoricalWorkouts={this.props.allHistoricalWorkouts} />
        </div>
      );
    } else {
      return <div></div>;
    }
  }
});

module.exports = RoutinesHome;
