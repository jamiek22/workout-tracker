import React from 'react';

var DaysAgo = React.createClass({
  getDaysAgo: function(dateString){
    var oneDay = 24*60*60*1000;
    var today = new Date();
    var testDate = new Date(dateString);
    return Math.round(Math.abs((today.getTime() - testDate.getTime())/oneDay));
  },

  render: function(){
    return <span>{this.getDaysAgo(this.props.date)} days ago</span>;
  }
});

module.exports = DaysAgo;
