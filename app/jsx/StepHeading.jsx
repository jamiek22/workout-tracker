import React from 'react';

var StepHeading = React.createClass({
  render: function(){
    return (
        <h2>{this.props.name} <small>({this.props.stepNumber}/{this.props.totalSteps})</small></h2>
      );
  }
});

module.exports = StepHeading;
