import React from 'react';
import ReactDOM from 'react-dom';

var RoutinesHome = require('./jsx/RoutinesHome.jsx');
var RoutineStep = require('./jsx/RoutineStep.jsx');

var Main = React.createClass({
  getInitialState() {
    return {
      loadedRoutine: null,
      loadedRoutineStep: 0,
      routines: [],
      newWorkout: new Workout(),
      allHistoricalWorkouts: [],
    };
  },

  goToNextStep(){
    //Go to the next step
    this.setState({ loadedRoutineStep: this.state.loadedRoutineStep+1 });
  },

  goToPreviousStep(){
    var newStep = this.state.loadedRoutineStep - 1;
    if (newStep >= 0) {
      this.setState({ loadedRoutineStep: this.state.loadedRoutineStep-1 });
    } else {
      this.setState({ loadedRoutine: null });
    }
  },

  initializeRoutines() {
    var routines = [];

    var deltsAndBicepsRoutine = new Routine("Delts and Biceps");
    deltsAndBicepsRoutine.addCardio();
    deltsAndBicepsRoutine.addSuperSet("Standing DB Curls", "Seated front military DB Press", 3, 12);
    deltsAndBicepsRoutine.addSuperSet("Standing DB Hammer Curl", "Upright DB Row", 3, 12);
    deltsAndBicepsRoutine.addExercise("Abs", 1, 99, false);
    deltsAndBicepsRoutine.addSuperSet("Preacher Curls", "Standing DB Lateral Raise", 3, 12);
    deltsAndBicepsRoutine.addSuperSet("BB Wide Curl", "Reverse Flyes DB", 3, 12);
    deltsAndBicepsRoutine.addExercise("Obliques", 1, 99, false);
    deltsAndBicepsRoutine.addCardio();
    deltsAndBicepsRoutine.addStretching();
    routines.push(deltsAndBicepsRoutine);

    var chestAndTrisRoutine = new Routine("Chest and Triceps");
    chestAndTrisRoutine.addCardio();
    chestAndTrisRoutine.addSuperSet("DB Flat Bench Press", "Rope Press-Downs", 3, 12);
    chestAndTrisRoutine.addSuperSet("Incline DB Flyes", "DB Incline Skull-Crushers", 3, 12);
    chestAndTrisRoutine.addExercise("Abs", 1, 99, false);
    chestAndTrisRoutine.addSuperSet("Cable Cross-Overs", "Push-Ups", 3, 12);
    chestAndTrisRoutine.addSuperSet("DB Incline Bench Press", "DB Tricep Extensions", 3, 12);
    chestAndTrisRoutine.addExercise("Obliques", 1, 99, false);
    chestAndTrisRoutine.addCardio();
    chestAndTrisRoutine.addStretching();
    routines.push(chestAndTrisRoutine);

    var backRoutine = new Routine("Back");
    backRoutine.addCardio();
    backRoutine.addSuperSet("Hyperextensions", "V-Grip Pull-Downs", 3, 12);
    backRoutine.addSuperSet("Bent-Over DB Rows", "Wide-Grip Lat Pull-Downs", 3, 12);
    backRoutine.addExercise("Abs", 1, 99, false);
    backRoutine.addSuperSet("Wide-Grip T-Bar Rows", "Seated Cable Rows", 3, 12);
    backRoutine.addSuperSet("Behind The Neck Pull-Downs", "Obliques", 3, 12);
    backRoutine.addCardio();
    backRoutine.addStretching();
    routines.push(backRoutine);

    // 5x5
    var delts5 = new Routine("5s Delts", "danger");
    delts5.addCardio();
    delts5.addExercise("Seated front military DB Press", 5, 5);
    delts5.addExercise("Upright DB Row", 5, 5);
    delts5.addExercise("Standing DB Lateral Raise", 5, 5);
    delts5.addExercise("Reverse Flyes DB", 5, 5);
    routines.push(delts5);

    var bis5 = new Routine("5s Bis", "danger");
    bis5.addCardio();
    bis5.addExercise("Standing DB Hammer Curl", 5, 5);
    bis5.addExercise("Preacher Curls", 5, 5);
    bis5.addExercise("BB Wide Curl", 5, 5);
    routines.push(bis5);

    var chest5 = new Routine("5s Chest", "danger");
    chest5.addCardio();
    chest5.addSuperSet("DB Flat Bench Press", 5, 5);
    chest5.addSuperSet("Incline DB Flyes", 5, 5);
    chest5.addSuperSet("Cable Cross-Overs", 5, 5);
    chest5.addSuperSet("DB Incline Bench Press", 5, 5);
    routines.push(chest5);

    var back5 = new Routine("5s Back", "danger");
    back5.addCardio();
    back5.addSuperSet("V-Grip Pull-Downs", 5, 5);
    back5.addSuperSet("Bent-Over DB Rows", 5, 5);
    back5.addSuperSet("Wide-Grip Lat Pull-Downs", 5, 5);
    back5.addSuperSet("Wide-Grip T-Bar Rows", 5, 5);
    routines.push(back5);

    // Cardio
    var cardio1 = new Routine("500-Rep Challenge #1", "warning");
    cardio1.addCardio();
    cardio1.addExercise("Burpees", 1, 30);
    cardio1.addExercise("Jump Lunges", 1, 30);
    cardio1.addExercise("Mtn Climber Pushups", 1, 30);
    cardio1.addExercise("Toe Taps", 1, 30);
    cardio1.addExercise("Ab Bikes", 1, 100);
    cardio1.addExercise("Tuck Jumps", 1, 30);
    cardio1.addExercise("Sit Ups", 1, 30);
    cardio1.addExercise("X Hops", 1, 30);
    cardio1.addExercise("Commandos", 1, 30);
    cardio1.addExercise("Mtn Climber", 1, 100);
    cardio1.addExercise("Box Jumps", 1, 30);
    cardio1.addExercise("W8d Str8 Leg Jack-knifes", 1, 30);
    routines.push(cardio1);

    var cardio2 = new Routine("500-Rep Challenge #2", "warning");
    cardio2.addCardio();
    cardio2.addExercise("Med. Ball Sqt and Press", 1, 30);
    cardio2.addExercise("Tricep Dips", 1, 30);
    cardio2.addExercise("Skipping", 1, 100);
    cardio2.addExercise("Snap Jumps", 1, 30);
    cardio2.addExercise("Reverse Lunge + Knee Lift", 1, 30);
    cardio2.addExercise("Drop Push-Ups", 1, 30);
    cardio2.addExercise("Single Arm Squat and Press", 1, 30);
    cardio2.addExercise("X Mtn Climber", 1, 100);
    cardio2.addExercise("Split Jumps", 1, 30);
    cardio2.addExercise("W8d Bent Leg Jack-knifes", 1, 30);
    cardio2.addExercise("Jump Squats", 1, 30);
    cardio2.addExercise("Lay Down Push-Ups", 1, 30);
    routines.push(cardio2);

    // var tester = new Routine("tester");
    // tester.addCardio();
    // tester.addExercise("Hyperextensions", 1, 15, true);
    // tester.addStretching();
    // routines.push(tester);

    this.setState({routines: routines});
  },

  getHistoricalWorkouts() {
    this.setState({ allHistoricalWorkouts: JSON.parse(localStorage.getItem('workouts')) || []});

    // var thisCopy = this;
    // $.get("workouts.json", function( data ) {
    //   thisCopy.setState({allHistoricalWorkouts: data});
    //   console.log(thisCopy.state.allHistoricalWorkouts);
    // });
  },

  componentWillMount(){
    this.initializeRoutines();
    this.getHistoricalWorkouts();
  },

  startRoutine: function(routineName){
    this.setState({ loadedRoutine: routineName});

    var routineIndex = 0;
    //loop over them until find a match... this isnt the best approach
    for (var i = 0; i < this.state.routines.length; i++){
      if (this.state.routines[i].name === routineName) {
        routineIndex = i;
        break;
      }
    }

    var newWorkout = this.state.newWorkout;
    newWorkout.routine = this.state.routines[routineIndex];
    newWorkout.date = new Date();
    this.setState({ newWorkout: newWorkout });
  },

  handleChangeReps: function(value){
    var newWorkout = this.state.newWorkout;
    newWorkout.routine.actions[this.state.loadedRoutineStep].reps = value;
    this.setState({ newWorkout: newWorkout });
  },

  handleChangeWeight: function(value){
    var newWorkout = this.state.newWorkout;
    newWorkout.routine.actions[this.state.loadedRoutineStep].weight = value;
    this.setState({ newWorkout: newWorkout });
  },

  saveWorkout: function(){
    var allHistoricalWorkouts = this.state.allHistoricalWorkouts;
    allHistoricalWorkouts.unshift(this.state.newWorkout);  //prepend

    // $.ajax({
    //   type: "POST",
    //   data: {
    //     workouts: JSON.stringify(allHistoricalWorkouts)
    //   },
    //   dataType: "text",
    //   url: "workouts.php",
    //   success: function(json) {
    //     //go to home page
    //     this.setState({ loadedRoutine: null });

    //     //empty out anything?
    //   },
    //   error: function(jqXHR, textStatus, errorThrown) {
    //     alert(errorThrown);
    //   }
    // });

    localStorage.setItem('workouts', JSON.stringify(allHistoricalWorkouts));
    this.setState({ loadedRoutine: null });
    this.setState({ loadedRoutineStep: 0 });
  },

  render: function() {
    return (
      <div>
        <RoutinesHome loadedRoutine={this.state.loadedRoutine}
                      startRoutine={this.startRoutine}
                      allHistoricalWorkouts={this.state.allHistoricalWorkouts}
                      routines={this.state.routines} />

        <RoutineStep loadedRoutine={this.state.loadedRoutine} 
                     loadedRoutineStep={this.state.loadedRoutineStep}
                     goToNextStep={this.goToNextStep}
                     goToPreviousStep={this.goToPreviousStep}
                     handleChangeReps={this.handleChangeReps}
                     handleChangeWeight={this.handleChangeWeight}
                     routines={this.state.routines}
                     saveWorkout={this.saveWorkout}
                     allHistoricalWorkouts={this.state.allHistoricalWorkouts}
                     newWorkout={this.state.newWorkout} />
      </div>
    );
  }
});


ReactDOM.render(<Main /> , document.getElementById('app'));
