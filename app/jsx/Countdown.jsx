import React from 'react';

var Countdown = React.createClass({
  getInitialState() {
    return {
      duration: this.props.duration
    };
  },

  // When element is added to screen
  componentDidMount(){
    var thisCopy = this;
    var myInterval = setInterval(function(){
      thisCopy.setState({ duration: thisCopy.state.duration-1 });
    }, 1000);
    this.setState({myInterval: myInterval});  //so can later destroy it
  },

  // When element is removed from DOM
  componentWillUnmount(){
    clearInterval(this.state.myInterval);
  },

  // When two timers are called in a row, this gets called (the other methods dont).
  componentWillReceiveProps(newProps){
    this.setState({ duration: newProps.duration });
  },

  render: function(){
    return (
        <div className="large-12 columns timer">
          {this.state.duration}
        </div>
      );
  }
});

module.exports = Countdown;
